package kacper.barszczewski.notebook;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kacper.barszczewski.notebook.database.NotesInterface;
import kacper.barszczewski.notebook.dto.Notebook;

public class NotesListAdapter extends RecyclerView.Adapter<NotesListAdapter.NotebookItemHolder> {

    private static DateFormat formtter = SimpleDateFormat.getDateInstance();

    private NotesInterface nInterface;

    private Resources resources;
    private NotesListActivity activity;

    private int itemCount = 0;
    private List<Notebook> notebookList;
    private TextView noNotes;

    private int sortOption, typeOption = 2, orderOption = 1;
    private String lastQuerySearch = "";

    NotesListAdapter(NotesListActivity activity) {
//        this.nInterface = nInterface;
        noNotes = activity.findViewById(R.id.noNotebooks);
        this.activity = activity;
//        refreshItems();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.resources = recyclerView.getResources();
        Log.d("NotesListAdapter", "onAttachedToRecyclerView: ");
    }

    @NonNull
    @Override
    public NotebookItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item, parent, false);
        return new NotebookItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotebookItemHolder holder, int position) {
        holder.bind(notebookList.get(position));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            holder.itemView.setBackgroundColor(position % 2 == 0 ? resources.getColor(R.color.note_dark, null) : resources.getColor(R.color.note_light, null));
        } else {
            holder.itemView.setBackgroundColor(position % 2 == 0 ? resources.getColor(R.color.note_dark) : resources.getColor(R.color.note_light));
        }
    }

    @Override
    public int getItemCount() {
        return itemCount;
    }


    public void refreshItems() {
        refreshItems(null);
    }

    @SuppressLint("CheckResult")
    public void refreshItems(Runnable runAfter) {
        Observable.just(nInterface)
                .doOnNext(notesInterface -> notebookList = notesInterface.getItems())
                .subscribeOn(Schedulers.newThread())
                .map(NotesInterface::getSize)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integerObservable -> {
                    itemCount = integerObservable;
                    if (itemCount == 0) {
                        noNotes.setVisibility(View.VISIBLE);
                    } else {
                        noNotes.setVisibility(View.INVISIBLE);
                    }
                    sortAndOrder(orderOption, sortOption);
                    filtrNotesByType(typeOption);
                    if (runAfter != null) {
                        runAfter.run();
                    }
                });
    }

    @SuppressLint("CheckResult")
    private void removeItem(Notebook notebook) {
        Observable.just(nInterface)
                .doOnNext(notesInterface -> notesInterface.remove(notebook))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(notesInterface -> {
                    int pos = notebookList.indexOf(notebook);
                    notebookList.remove(pos);
                    itemCount--;
                    notifyItemRemoved(pos);
                });
    }

    public void setSortBy(int i) {
        this.sortOption = i;
        sortAndOrder(orderOption, sortOption);
        notifyDataSetChanged();
    }

    public void setOrder(int i) {
        this.orderOption = i;
        sortAndOrder(orderOption, sortOption);
        notifyDataSetChanged();
    }

    private void sortAndOrder(int order, int sort) {
        if (notebookList == null) {
            return;
        }
        int i = order == 0 ? 1 : -1;
        Collections.sort(notebookList, (o1, o2) -> {
            if (sort == 1) {
                return i * o1.getTitle().compareToIgnoreCase(o2.getTitle());
            } else if (sort == 0) {
                return i * o1.getEditDate().compareTo(o2.getEditDate());
            } else {
                return i * o1.getCreateDate().compareTo(o2.getCreateDate());
            }
        });
    }

    public void setNotesType(int i) {
        this.typeOption = i;
        filtrNotesByTypeAsync();
    }

    @SuppressLint("CheckResult")
    private void filtrNotesByTypeAsync() {
        Observable.just(nInterface)
                .doOnNext(notesInterface -> notebookList = notesInterface.getItems())
                .subscribeOn(Schedulers.newThread())
                .map(NotesInterface::getSize)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integerObservable -> {
                    filtrNotesByType(typeOption);
                    notifyDataSetChanged();
                });
    }

    private void filtrNotesByType(int type) {
        if (notebookList == null) {
            return;
        }
        ArrayList<Notebook> toRemove = new ArrayList<>();
        String query = lastQuerySearch.toLowerCase();
        for (Notebook notebook : notebookList) {
            if (type == 0 && notebook.getType().equals(Notebook.Type.CHECKBOX_NOTE)) {
                toRemove.add(notebook);
            } else if (type == 1 && notebook.getType().equals(Notebook.Type.TEXT_NOTE)) {
                toRemove.add(notebook);
            } else if (query.length() > 0 && !notebook.getTitle().toLowerCase().contains(query)) {
                toRemove.add(notebook);
            }
        }
        notebookList.removeAll(toRemove);
        itemCount = notebookList.size();
        if (itemCount == 0) {
            noNotes.setVisibility(View.VISIBLE);
        } else {
            noNotes.setVisibility(View.INVISIBLE);
        }
        notifyDataSetChanged();
    }

    public void setInterface(NotesInterface notesInterface) {
        this.nInterface = notesInterface;
    }

    public void clearFilters() {
        sortOption = 0;
        typeOption = 2;
        orderOption = 1;
        refreshItems();
    }

    @SuppressLint("CheckResult")
    public void filtrQuery(final String query) {
        if (lastQuerySearch.equals(query)) {
            return;
        }
        lastQuerySearch = query;
        if (query.length() == 0) {
            refreshItems();
            return;
        }
        refreshItems();

    }

    class NotebookItemHolder extends RecyclerView.ViewHolder {

        private TextView itemTextView, itemDate;

        private ImageView itemIconView;

        public NotebookItemHolder(@NonNull View itemView) {
            super(itemView);
            itemTextView = itemView.findViewById(R.id.item_text);
            itemIconView = itemView.findViewById(R.id.item_icon);
            itemDate = itemView.findViewById(R.id.item_date);
        }

        public void bind(Notebook notebook) {
            itemTextView.setText(notebook.getTitle());
            itemDate.setText(formtter.format(notebook.getCreateDate()));
            itemIconView.setImageResource(R.drawable.ic_check);
            itemIconView.setVisibility(View.INVISIBLE);
            if (!notebook.getType().equals(Notebook.Type.CHECKBOX_NOTE)) {
            } else {
                itemIconView.setVisibility(View.VISIBLE);
            }
            itemView.setOnClickListener(v -> activity.showNotebookActivity(notebook));
            itemView.setOnLongClickListener(v -> {
                new AlertDialog.Builder(activity)
                        .setTitle(notebook.getTitle())
                        .setMessage(R.string.confirm_remove)
                        .setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss())
                        .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                            removeItem(notebook);
                        }).show();
                return true;
            });
        }

    }

}
