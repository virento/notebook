package kacper.barszczewski.notebook;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.util.Strings;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kacper.barszczewski.notebook.database.DatabaseHelper;
import kacper.barszczewski.notebook.database.NotesInterface;
import kacper.barszczewski.notebook.dto.NoteAddress;
import kacper.barszczewski.notebook.dto.Notebook;

public abstract class NoteActivityAbstract extends AppCompatActivity implements LocationListener {

    public static final String NOTE_ID = "kacper.barszczewski.NOTE_ID";
    public static final String NOTE_DB_TYPE = "kacper.barszczewski.NOTE_DB_TYPE";
    public static final String EDIT_MODE_ON = "kacper.berszczewski.EDIT_MODE_ON";
    public static final int LOC_PERMISSION_REQUEST = 123;

    protected NotesInterface nInterface;

    protected TextView locAddText;
    protected TextView locText;

    protected FusedLocationProviderClient fusedLocationProviderClient;

    protected AddressResultReceiver resultReceiver;

    protected Notebook notebook;

    protected FloatingActionButton fab;

    protected Boolean shouldBeSaved = false;

    private LocationManager locationManager;

    private ProgressBar loadingIcon;

    private View rootView;

    @SuppressLint("CheckResult")
    protected void init(Bundle savedInstanceState) {
        long noteId = getIntent().getLongExtra(NOTE_ID, -1);
        int noteDbType = getIntent().getIntExtra(NOTE_DB_TYPE, -1);
        if (noteId == -1 || noteDbType == -1) {
            finishActivity(0);
            return;
        }

        fab = findViewById(R.id.note_fab);
        fab.setTag(false);
        fab.setOnClickListener(v -> {
            if (!(boolean) fab.getTag()) {
                onEditModeEnable();
            } else {
                onEditModeDisable();
            }
        });

        nInterface = DatabaseHelper.getInterface(DatabaseHelper.DatabaseType.fromValue(noteDbType));
        if (nInterface == null) {
            finish();
            return;
        }

        rootView = findViewById(R.id.root_view);
        locAddText = findViewById(R.id.loc_add_text);
        locText = findViewById(R.id.note_loc);
        loadingIcon = findViewById(R.id.loadingIcon);
        loadingIcon.setVisibility(View.INVISIBLE);

        Boolean editModeOn = null;
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(EDIT_MODE_ON)) {
                editModeOn = savedInstanceState.getBoolean(EDIT_MODE_ON);
            }
        }

        if (noteId == -2) {
            notebook = createNotebook();
            onNotebookFetched();
            if (editModeOn == null) {
                editModeOn = true;
            }
        } else {
            Observable.just(nInterface)
                    .subscribeOn(Schedulers.newThread())
                    .map(notesInterface -> notesInterface.getNoteById(noteId))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(note -> {
                        notebook = note;
                        onNotebookFetched();
                    });
            if (editModeOn == null) {
                editModeOn = false;
            }
        }
        if (editModeOn) {
            onEditModeEnable();
        } else {
            onEditModeDisable();
        }

        resultReceiver = new AddressResultReceiver(new Handler(Looper.getMainLooper()));

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locAddText.setOnClickListener(v -> {
            if (!checkPermission()) {
                return;
            }
            if (Strings.isEmptyOrWhitespace(locText.getText().toString())) {
                getLasLocation();
                locAddText.setVisibility(View.INVISIBLE);
                loadingIcon.setVisibility(View.VISIBLE);
                loadingIcon.setIndeterminate(true);
            } else {
                removeAddressFromNote();
            }
        });
        locText.setOnClickListener(v -> {
            if (Strings.isEmptyOrWhitespace(locText.getText().toString())) {
                return;
            }
            NoteAddress noteAdr = getNotebook().getAddress();
            showLocationInMapView(noteAdr.getLatitude(), noteAdr.getLongitude());
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EDIT_MODE_ON, (Boolean) fab.getTag());
    }

    protected void onNotebookFetched() {
        updateLocationText(notebook.getAddress());
    }

    protected void onEditModeDisable() {
        fab.setTag(false);
        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit));
        locAddText.setVisibility(View.INVISIBLE);
    }

    protected void onEditModeEnable() {
        shouldBeSaved = true;
        fab.setTag(true);
        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
        locAddText.setVisibility(View.VISIBLE);
    }

    protected boolean inEditMode() {
        return (boolean) fab.getTag();
    }

    protected abstract Notebook createNotebook();

    protected void showLocationInMapView(Double latitude, Double longitude) {
        if (latitude == null || longitude == null) {
            return;
        }
        Uri intentUri = Uri.parse("geo:" + latitude.toString() + "," + longitude.toString());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, intentUri);
        startActivity(mapIntent);
    }

    protected void removeAddressFromNote() {
        NoteAddress noteAdr = getNotebook().getAddress();
        noteAdr.setLocality(null);
        noteAdr.setLongitude(null);
        noteAdr.setLatitude(null);
        noteAdr.setCountry(null);
        locText.setText(null);
        locAddText.setText(R.string.add_location);
    }

    @Override
    public void onLocationChanged(Location location) {
        startIntentService(location);
        locAddText.setEnabled(false);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("NoteActivityAbstract", "Location is disabled!");
        locAddText.setVisibility(View.VISIBLE);
        loadingIcon.setVisibility(View.INVISIBLE);
        new AlertDialog.Builder(this)
                .setTitle(R.string.location_required)
                .setMessage(R.string.location_required_msg)
                .setNegativeButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @SuppressLint("MissingPermission")
    protected void getLasLocation() {
        if (locationManager != null) {
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, Looper.getMainLooper());
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(command1 -> {
            if (command1 == null) {
                Log.d("NoteActivityAbstract", "Location is null");
                new AlertDialog.Builder(this)
                        .setMessage(R.string.location_error)
                        .show();
                return;
            }
            startIntentService(command1);
            locAddText.setEnabled(false);
        });
    }

    protected boolean checkPermission() {
        List<String> permissions = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissions.size() == 0) {
            return true;
        }

        ActivityCompat.requestPermissions(this,
                permissions.toArray(new String[0]), LOC_PERMISSION_REQUEST);


        return false;
    }

    protected Notebook getNotebook() {
        return notebook;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOC_PERMISSION_REQUEST:
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLasLocation();
                }
                break;

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    protected void startIntentService(Location lastLocation) {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(FetchAddressIntentService.RECEIVER, resultReceiver);
        intent.putExtra(FetchAddressIntentService.LOCATION_DATA_EXTRA, lastLocation);
        startService(intent);
    }

    protected void updateLocationText(NoteAddress address) {
        if (Strings.isEmptyOrWhitespace(address.getLocality()) || Strings.isEmptyOrWhitespace(address.getCountry())) {
            locAddText.setText(R.string.add_location);
            return;
        }
        locAddText.setText(R.string.rem_location);
        locText.setText(address.getLocality() + ", " + address.getCountry());
        loadingIcon.setVisibility(View.INVISIBLE);
        locAddText.setVisibility(View.VISIBLE);
    }

    protected class AddressResultReceiver extends ResultReceiver {

        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            locAddText.setEnabled(true);
            if (resultData == null) {
                return;
            }

            String resultMsg = resultData.getString(FetchAddressIntentService.RESULT_MESSAGE_KEY, "");
            Log.d("NoteActivityAbstract", "onReceiveResult: address: " + resultMsg);

            Address address = resultData.getParcelable(FetchAddressIntentService.ADDRESS_EXTRA);
            if (resultCode == FetchAddressIntentService.SUCCESS_RESULT && address != null) {
                Toast.makeText(NoteActivityAbstract.this, getString(R.string.address_found), Toast.LENGTH_SHORT).show();
                NoteAddress noteAdr = getNotebook().getAddress();
                noteAdr.setCountry(address.getCountryName());
                noteAdr.setLocality(address.getLocality());
                noteAdr.setLatitude(address.getLatitude());
                noteAdr.setLongitude(address.getLongitude());
                updateLocationText(noteAdr);
            }
        }
    }

    public void disableEditText(EditText editText) {
        rootView.requestFocus();
        editText.setCursorVisible(false);
        editText.setOnTouchListener((v, event) -> true);
    }

    public void enableEditText(EditText editText) {
        editText.setOnTouchListener(null);
        editText.setCursorVisible(true);
    }
}
