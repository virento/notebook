package kacper.barszczewski.notebook;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.Date;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kacper.barszczewski.notebook.dto.Notebook;
import kacper.barszczewski.notebook.dto.TextNote;

public class TextNoteActivity extends NoteActivityAbstract {

    private TextNote notebook;
    private EditText titleText;
    private EditText bodyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_note);
        titleText = findViewById(R.id.note_title);
        bodyText = findViewById(R.id.note_body);

        init(savedInstanceState);

        if (super.notebook != null) {
            if (!(super.notebook instanceof TextNote)) {
                finish();
                return;
            }
            if (notebook == null) {
                this.notebook = (TextNote) super.notebook;
            }
        }
    }

    @Override
    protected void onNotebookFetched() {
        super.onNotebookFetched();
        if (!(super.notebook instanceof TextNote)) {
            finish();
            return;
        }

        notebook = (TextNote) super.notebook;

        titleText.setText(notebook.getTitle());
        bodyText.setText(notebook.getBody());
    }

    @Override
    public void onBackPressed() {
        if(!shouldBeSaved) {
            super.onBackPressed();
            return;
        }
        saveNote();
        if (notebook.getTitle().length() == 0) {
            String body = notebook.getBody();
            if (body.length() == 0) {
                super.onBackPressed();
                return;
            } else {
                int length = 10;
                if (body.length() < 10) {
                    length = body.length();
                }
                notebook.setTitle(body.subSequence(0, length).toString());
            }
        }

        Observable.just(nInterface)
                .doOnNext(notesInterface -> notesInterface.save(notebook))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(notesInterface -> super.onBackPressed());
    }

    private void saveNote() {
        notebook.setTitle(titleText.getText().toString());
        notebook.setBody(bodyText.getText().toString());
    }

    @Override
    protected Notebook createNotebook() {
        TextNote note = new TextNote();
        note.setCreateDate(new Date());
        return note;
    }

    @Override
    protected void onEditModeDisable() {
        super.onEditModeDisable();
        disableEditText(bodyText);
        disableEditText(titleText);
    }

    @Override
    protected void onEditModeEnable() {
        super.onEditModeEnable();
        enableEditText(bodyText);
        enableEditText(titleText);
    }

    @Override
    protected Notebook getNotebook() {
        return notebook;
    }
}
