package kacper.barszczewski.notebook.database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import kacper.barszczewski.notebook.dto.Checkbox;
import kacper.barszczewski.notebook.dto.CheckboxNote;
import kacper.barszczewski.notebook.dto.Notebook;
import kacper.barszczewski.notebook.dto.TextNote;

public class SimpleDatabaseImpl implements NotesInterface {

    private static SimpleDatabaseImpl instance;

    public static SimpleDatabaseImpl getInstance() {
        if (instance == null) {
            instance = new SimpleDatabaseImpl(15);
        }
        return instance;
    }

    private final int NOTEBOOKS_SIZE;

    private List<Notebook> noteList = new ArrayList<>();

    private Random random = new Random();

    private int checkboxLasId = 0;

    private SimpleDatabaseImpl(int size) {
        NOTEBOOKS_SIZE = size;
        generate();
        sort();
    }

    private void generate() {
        for (int i = 0; i < NOTEBOOKS_SIZE; i++) {
            if (i % 2 == 0) {
                noteList.add(generateTextNotebook());
            } else {
                noteList.add(generateCheckboxNotebook());
            }
        }
    }

    private Notebook generateCheckboxNotebook() {
        CheckboxNote notebook = new CheckboxNote();
        notebook.setTitle("Checkbox number " + noteList.size());
        notebook.setCreateDate(new Date(System.currentTimeMillis() - random.nextInt(1000000000)));
        notebook.setId((long) noteList.size());
        for (int i = 0; i < random.nextInt(10) + 10; i++) {
            Checkbox checkbox = new Checkbox();
            checkbox.setId((long) checkboxLasId++);
            checkbox.setChecked(random.nextBoolean());
            checkbox.setNotebookId(notebook.getId());
            checkbox.setText("Checkbox nr " + i);
            notebook.getCheckboxes().add(checkbox);
        }
        return notebook;
    }

    private Notebook generateTextNotebook() {
        TextNote notebook = new TextNote();
        notebook.setCreateDate(new Date(System.currentTimeMillis() - random.nextInt(1000000000)));
        notebook.setId((long) noteList.size());
        notebook.setTitle("Text number " + noteList.size());
        notebook.setBody(" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ipsum justo, fringilla laoreet sem sed, hendrerit elementum enim. Duis lacinia quam vel libero sollicitudin, ut sagittis diam ullamcorper. Vestibulum auctor malesuada nisl, a semper nulla. Cras placerat sagittis orci a volutpat. Vestibulum varius dapibus tortor. Nam quis vulputate enim. Ut velit arcu, bibendum aliquet turpis a, euismod tempor risus. Integer fermentum venenatis urna et laoreet.\n" +
                "\n" +
                "Pellentesque at fermentum neque. Morbi et condimentum mauris. Sed eu mattis felis. Duis in porttitor dui. Pellentesque ornare, tortor at consectetur vehicula, mauris dolor convallis arcu, eu luctus purus eros a lacus. Nam quis aliquet massa. Curabitur in placerat ipsum. Donec id facilisis libero, eget elementum justo. Vivamus cursus pellentesque arcu eget rhoncus. Fusce mattis orci at est dignissim ultricies. Cras vestibulum aliquam ex, a mattis augue tincidunt vel. Pellentesque semper pulvinar lectus et lacinia. Integer ac dui a sapien malesuada malesuada id ac felis. Donec ut est dolor. ");
        return notebook;
    }

    @Override
    public Notebook getNote(int position) {
        return noteList.get(position);
    }

    @Override
    public Notebook getNoteById(Long id) {
        for (int i = 0; i < noteList.size(); i++) {
            if (noteList.get(i).getId().equals(id)) {
                return noteList.get(i);
            }
        }
        return null;
    }

    @Override
    public int getSize() {
        return noteList.size();
    }

    public void sort() {
        Collections.sort(noteList, (o1, o2) -> -1 * o1.getCreateDate().compareTo(o2.getCreateDate()));
    }

    @Override
    public void save(Notebook notebook) {
        if (!noteList.contains(notebook)) {
            notebook.setId(UUID.randomUUID().getMostSignificantBits());
            noteList.add(notebook);
            sort();
        }
    }

    @Override
    public DatabaseHelper.DatabaseType getType() {
        return DatabaseHelper.DatabaseType.SAMPLES;
    }

    @Override
    public ArrayList<Notebook> getItems() {
        return null;
    }

    @Override
    public void close() {

    }

    @Override
    public void remove(Notebook dbNote) {

    }
}
