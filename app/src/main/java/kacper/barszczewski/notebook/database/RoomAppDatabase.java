package kacper.barszczewski.notebook.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import kacper.barszczewski.notebook.dao.CheckboxNoteDao;
import kacper.barszczewski.notebook.dao.Converters;
import kacper.barszczewski.notebook.dao.NoteDao;
import kacper.barszczewski.notebook.dao.NotebookDao;
import kacper.barszczewski.notebook.dao.TextNoteDao;
import kacper.barszczewski.notebook.dto.Checkbox;
import kacper.barszczewski.notebook.dto.CheckboxNote;
import kacper.barszczewski.notebook.dto.Note;
import kacper.barszczewski.notebook.dto.NoteAddress;
import kacper.barszczewski.notebook.dto.TextNote;

@Database(entities = {TextNote.class, CheckboxNote.class, Checkbox.class, NoteAddress.class, Note.class}, version = 14)
@TypeConverters({Converters.class})
public abstract class RoomAppDatabase extends RoomDatabase {
    public abstract TextNoteDao textNoteDao();

    public abstract CheckboxNoteDao checkboxNoteDao();

    public abstract NotebookDao notebookDao();

    public abstract NoteDao noteDao();
}
