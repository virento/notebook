package kacper.barszczewski.notebook.database;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;

import kacper.barszczewski.notebook.dto.Checkbox;
import kacper.barszczewski.notebook.dto.CheckboxNote;
import kacper.barszczewski.notebook.dto.Notebook;
import kacper.barszczewski.notebook.dto.TextNote;

public class FirebaseDatabaseImpl implements NotesInterface {

    private static final String ROOM_DB_NAME = "firebase-notebook";

    private FirebaseFirestore db;
    private RoomAppDatabaseImpl roomDb;
    private String userId;

    private static FirebaseDatabaseImpl instance;

    public static FirebaseDatabaseImpl getInstance() {
        if (instance == null) {
            instance = new FirebaseDatabaseImpl();
        }
        return instance;
    }

    public static void init(Context context) {
        RoomAppDatabaseImpl.init(context, ROOM_DB_NAME);
        getInstance().roomDb = RoomAppDatabaseImpl.getInstance(ROOM_DB_NAME);
    }

    private FirebaseDatabaseImpl() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            userId = currentUser.getUid();
            db = FirebaseFirestore.getInstance();
            roomDb = RoomAppDatabaseImpl.getInstance(ROOM_DB_NAME);
        }
    }

    @Override
    public Notebook getNote(int position) {
        return roomDb.getNote(position);
    }

    @Override
    public Notebook getNoteById(Long id) {
        return roomDb.getNoteById(id);
    }

    @Override
    public int getSize() {
        return roomDb.getSize();
    }

    @Override
    public void save(Notebook notebook) {
        roomDb.save(notebook);
        getNotesReference().document(String.valueOf(notebook.getId())).set(notebook);
    }

    @Override
    public DatabaseHelper.DatabaseType getType() {
        return DatabaseHelper.DatabaseType.FIREBASE;
    }

    @Override
    public ArrayList<Notebook> getItems() {
        return roomDb.getItems();
    }

    @Override
    public void sync() {
        final Thread currThread = Thread.currentThread();
        getNotesReference()
                .get()
                .addOnCompleteListener(Runnable::run, task -> {
                    synchronized (currThread) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            processSynchronization(task.getResult());
                        }
                        currThread.notifyAll();
                    }
                });
        try {
            synchronized (currThread) {
                Log.d("FirebaseDatabaseImpl", "sync: Pausing currThread");
                currThread.wait();
                Log.d("FirebaseDatabaseImpl", "sync: Resuming currThread");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private CollectionReference getNotesReference() {
        return db.collection("notes")
                .document(userId).collection("private");
    }

    private void processSynchronization(QuerySnapshot reults) {
        List<DocumentSnapshot> documents = reults.getDocuments();
        List<Notebook> currentNotes = getItems();
        for (DocumentSnapshot document : documents) {
            Notebook fbNote;
            if (document.contains("type") && document.getString("type").equals(Notebook.Type.TEXT_NOTE)) {
                fbNote = document.toObject(TextNote.class);
            } else {
                fbNote = document.toObject(CheckboxNote.class);
            }
            if (fbNote == null) {
                continue;
            }
            Notebook dbNote = roomDb.getNoteById(fbNote.getId());
            if (dbNote == null) {
                roomDb.save(fbNote);
                Log.d("FirebaseDatabaseImpl", "sync: Creating missing note " + fbNote);
                continue;
            }
            if (dbNote.getEditDate().before(fbNote.getEditDate())) {
                if (!dbNote.getType().equals(fbNote.getType())) {
                    roomDb.remove(dbNote);
                }
                roomDb.save(fbNote);
                Log.d("FirebaseDatabaseImpl", "sync: Merging " + dbNote);
            }
            int removId = -1;
            for (int i = 0; i < currentNotes.size(); i++) {
                if (currentNotes.get(i).getId().equals(dbNote.getId())) {
                    removId = i;
                    break;
                }
            }
            if (removId >= 0) {
                currentNotes.remove(removId);
            }
        }
        for (Notebook currentNote : currentNotes) {
            roomDb.remove(currentNote);
        }
        Log.d("FirebaseDatabaseImpl", "sync: Resuming currThread from function");
    }

    @Override
    public void remove(Notebook dbNote) {
        getNotesReference().document(String.valueOf(dbNote.getId()))
                .delete();
        roomDb.remove(dbNote);
    }

    @Override
    public void close() {
        roomDb.close();
    }

    @Override
    public void clear() {
        roomDb.clear();
        getNotesReference().getParent().delete();
    }

    @Override
    public void performImport(RoomAppDatabaseImpl instance) {
        for (Notebook item : instance.getItems()) {
            item.setId(null);
            item.setAddressId(null);
            item.getAddress().setId(null);
            if (item.getType().equals(Notebook.Type.CHECKBOX_NOTE)) {
                for (Checkbox box : ((CheckboxNote) item).getCheckboxes()) {
                    box.setId(null);
                    box.setNotebookId(null);
                }
            }
            save(item);
        }
    }
}
