package kacper.barszczewski.notebook.database;


public class DatabaseHelper {

    public enum DatabaseType {
        SAMPLES(1),
        ROOM(2),
        FIREBASE(3);

        private int value;

        DatabaseType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DatabaseType fromValue(int value) {
            switch (value) {
                case 1:
                    return DatabaseType.SAMPLES;
                case 2:
                    return DatabaseType.ROOM;
                case 3:
                    return DatabaseType.FIREBASE;
            }
            return null;
        }
    }

    public static NotesInterface getInterface(DatabaseType type) {
        if (type == null) {
            return null;
        }
        switch (type) {
            case SAMPLES:
                return SimpleDatabaseImpl.getInstance();
            case ROOM:
                return RoomAppDatabaseImpl.getInstance();
            case FIREBASE:
                return FirebaseDatabaseImpl.getInstance();
        }
        return null;
    }
}
