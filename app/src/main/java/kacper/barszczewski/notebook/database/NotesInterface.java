package kacper.barszczewski.notebook.database;

import java.util.ArrayList;

import kacper.barszczewski.notebook.dto.Notebook;
import kacper.barszczewski.notebook.dto.TextNote;

public interface NotesInterface {

    Notebook getNote(int position);

    Notebook getNoteById(Long id);

    int getSize();

    void save(Notebook notebook);

    DatabaseHelper.DatabaseType getType();

    ArrayList<Notebook> getItems();

    void close();

    default void sync() {

    }

    default void clear() {

    }

    void remove(Notebook dbNote);

    default void performImport(RoomAppDatabaseImpl instance) {

    }
}
