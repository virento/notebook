package kacper.barszczewski.notebook.database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import kacper.barszczewski.notebook.dto.Checkbox;
import kacper.barszczewski.notebook.dto.CheckboxNote;
import kacper.barszczewski.notebook.dto.Note;
import kacper.barszczewski.notebook.dto.NoteAddress;
import kacper.barszczewski.notebook.dto.Notebook;
import kacper.barszczewski.notebook.dto.TextNote;

public class RoomAppDatabaseImpl implements NotesInterface {

    private static RoomAppDatabaseImpl instance;
    private static RoomAppDatabaseImpl extInstance;

    private static final String DB_NAME = "notebook-database";

    private RoomAppDatabase db;

    private List<Notebook> notebooks = new ArrayList<>();
    private boolean notesCacheValid = false;

    public static void init(Context context) {
        if(getInstance().db != null) {
            return;
        }
        getInstance().db = Room.databaseBuilder(context, RoomAppDatabase.class, DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public static void init(Context context, String dbName) {
        if (getInstance(dbName).db != null) {
            getInstance(dbName).db.close();
        }
        getInstance(dbName).db = Room.databaseBuilder(context, RoomAppDatabase.class, dbName)
                .fallbackToDestructiveMigration()
                .build();
    }

    public static RoomAppDatabaseImpl getInstance(String dbName) {
        if (extInstance == null) {
            extInstance = new RoomAppDatabaseImpl();
        }
        return extInstance;
    }

    public static RoomAppDatabaseImpl getInstance() {
        if (instance == null) {
            instance = new RoomAppDatabaseImpl();
        }
        return instance;
    }

    private RoomAppDatabaseImpl() {

    }

    @Override
    public Notebook getNote(int position) {
        updateNotebooksCache();
        if (position >= notebooks.size()) {
            return null;
        }
        return notebooks.get(position);
    }

    @Override
    public Notebook getNoteById(Long id) {
        updateNotebooksCache();
        Notebook result = null;
        for (Notebook notebook : notebooks) {
            if (notebook.getId().equals(id)) {
                result = notebook;
                break;
            }
        }
        if (result == null) {
            result = db.checkboxNoteDao().getById(id);
        }
        if (result == null) {
            result = db.textNoteDao().getById(id);
        }
        if (result == null) {
            return null;
        }

        initItem(result);

        return result;
    }

    private void initItem(Notebook result) {
        result.setAddress(db.notebookDao().getAddressById(result.getAddressId()));
        if (result instanceof CheckboxNote) {
            ((CheckboxNote) result).setCheckboxes(db.checkboxNoteDao().getCheckboxForNote(result.getId()));
        }
    }

    @Override
    public int getSize() {
        int size = 0;
        size += db.checkboxNoteDao().size();
        size += db.textNoteDao().size();
        return size;
    }

    @Override
    @Transaction
    public void save(Notebook notebook) {
        NoteAddress noteAddress = notebook.getAddress();
        Long addrId = noteAddress.getId();
        boolean presentInDb = db.noteDao().isNotePresent(notebook.getId()) == 1;

        if (noteAddress.getId() == null || !presentInDb) {
            addrId = db.notebookDao().insert(noteAddress);
            noteAddress.setId(addrId);
        } else {
            db.notebookDao().update(noteAddress);
        }
        if (notebook.getId() == null) {
            Note note = new Note();
            insertNote(notebook, note);
            Long id = db.noteDao().insert(note);
            note.setId(id);

            notebook.setEditDate(new Date());
            notebook.setId(note.getId());
            notebook.setAddressId(addrId);
            insertNotebook(notebook);
        } else if (presentInDb) {
            notebook.setEditDate(new Date());
            updateNotebook(notebook);
        } else {
            Note note = new Note();
            note.setId(notebook.getId());
            db.noteDao().insert(note);
            db.notebookDao().insert(notebook.getAddress());
            insertNotebook(notebook);
        }

        if (notebook.getType().equals(Notebook.Type.CHECKBOX_NOTE)) {
            for (Checkbox checkbox : ((CheckboxNote) notebook).getCheckboxes()) {
                if (checkbox.getId() == null) {
                    checkbox.setNotebookId(notebook.getId());
                    Long boxId = db.notebookDao().insert(checkbox);
                    checkbox.setId(boxId);
                } else {
                    db.notebookDao().update(checkbox);
                }
            }
        }
        notesCacheValid = false;
    }

    private void updateNotebook(Notebook notebook) {
        if (notebook instanceof TextNote) {
            db.notebookDao().update((TextNote) notebook);
        } else {
            db.notebookDao().update((CheckboxNote) notebook);
        }
    }

    private void insertNote(Notebook notebook, Note note) {
        if (notebook instanceof TextNote) {
            note.setType(Note.Type.TEXT_NOTE);
        } else {
            note.setType(Note.Type.CHECKBOX_NOTE);
        }
    }

    private void insertNotebook(Notebook notebook) {
        if (notebook instanceof TextNote) {
            db.notebookDao().insert((TextNote) notebook);
        } else {
            db.notebookDao().insert((CheckboxNote) notebook);
        }
    }

    @Override
    public DatabaseHelper.DatabaseType getType() {
        return DatabaseHelper.DatabaseType.ROOM;
    }

    @Override
    public ArrayList<Notebook> getItems() {
        if (notesCacheValid) {
            return new ArrayList<>(notebooks);
        }
        ArrayList<Notebook> list = new ArrayList<>();
        list.addAll(db.checkboxNoteDao().getAll());
        list.addAll(db.textNoteDao().getAll());
        for (Notebook notebook : list) {
            initItem(notebook);
        }
        Collections.sort(list, (o1, o2) -> -1 * o1.getEditDate().compareTo(o2.getEditDate()));
        return list;
    }

    @Override
    public void close() {
        db.close();
    }

    @Override
    public void remove(Notebook dbNote) {
        if (dbNote.getType().equals(Notebook.Type.TEXT_NOTE)) {
            db.notebookDao().delete((TextNote) dbNote);
        } else {
            db.notebookDao().delete((CheckboxNote) dbNote);
        }
        db.noteDao().delete(db.noteDao().getById(dbNote.getId()));
        notesCacheValid = false;
    }

    private void updateNotebooksCache() {
        if (notesCacheValid) return;
        notebooks = getItems();
        notesCacheValid = true;
    }

    @Override
    public void clear() {
        notesCacheValid = false;
        db.clearAllTables();
    }
}
