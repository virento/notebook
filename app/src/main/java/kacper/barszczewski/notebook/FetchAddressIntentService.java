package kacper.barszczewski.notebook;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.android.gms.common.internal.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FetchAddressIntentService extends IntentService {

    public static final String RESULT_DATA_KEY = "kacper.barszczewski.RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = "kacper.barszczewski.LOCATION_DATA_EXTRA";
    public static final String RECEIVER = "kacper.barszczewski.RECEIVER";
    public static final String ADDRESS_EXTRA = "kaacper.barszczewski.ADDRESS_EXTRA";
    public static final String RESULT_MESSAGE_KEY = "kacper.barszczewski.RESULT_MESSAGE_KEY";
    public static final int FAILURE_RESULT = 1;
    public static final int SUCCESS_RESULT = 0;

    private static final String TAG = "FetchAddressIntent";

    protected ResultReceiver receiver;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public FetchAddressIntentService(String name) {
        super(name);
    }

    public FetchAddressIntentService() {
        super(null);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }

        receiver = intent.getParcelableExtra(FetchAddressIntentService.RECEIVER);

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        String errorMessage = "";
        Location location = intent.getParcelableExtra(
                FetchAddressIntentService.LOCATION_DATA_EXTRA);

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException exception) {
            errorMessage = getString(R.string.service_not_available);
            Log.e(TAG, errorMessage, exception);
        } catch (IllegalArgumentException illegalArgumentException) {
            errorMessage = getString(R.string.invalid_lat_long_used);
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + location.getLatitude() +
                    ", Longitude = " +
                    location.getLongitude(), illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(TAG, errorMessage);
            }
            deliverResultToReceiver(FetchAddressIntentService.FAILURE_RESULT, errorMessage, null);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<>();

            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }
            Log.i(TAG, "NoteAddress found");
            deliverResultToReceiver(FetchAddressIntentService.SUCCESS_RESULT,
                    TextUtils.join("\n",
                            addressFragments), address);
        }


    }

    private void deliverResultToReceiver(int resultCode, String message, Address address) {
        Bundle bundle = new Bundle();
        bundle.putString(FetchAddressIntentService.RESULT_MESSAGE_KEY, message);
        if (address != null) {
            bundle.putParcelable(FetchAddressIntentService.ADDRESS_EXTRA, address);
        }
        receiver.send(resultCode, bundle);
    }

}
