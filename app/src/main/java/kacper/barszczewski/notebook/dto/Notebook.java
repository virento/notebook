package kacper.barszczewski.notebook.dto;

import androidx.room.ColumnInfo;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Notebook {

    public static interface Type {
        public static String TEXT_NOTE = "TN";
        public static String CHECKBOX_NOTE = "CN";
    }

    @PrimaryKey
    private Long id;

    @ColumnInfo(name = "create_date")
    private Date createDate;

    @ColumnInfo(name = "last_edit")
    private Date editDate;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "addressId")
    private Long addressId;

    @Ignore
    private NoteAddress address = new NoteAddress();

    @Ignore
    private String type;

    public void setCreateDate(Date date) {
        this.createDate = date;
        if (this.editDate == null) {
            this.editDate = (Date) date.clone();
        }
    }
}
