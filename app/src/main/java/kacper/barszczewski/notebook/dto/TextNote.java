package kacper.barszczewski.notebook.dto;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import lombok.Getter;
import lombok.Setter;

import static kacper.barszczewski.notebook.dto.Notebook.Type.TEXT_NOTE;

@Getter
@Setter
@Entity
public class TextNote extends Notebook {

    @ColumnInfo(name = "body")
    private String body;


    public TextNote() {
        setType(TEXT_NOTE);
    }
}
