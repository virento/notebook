package kacper.barszczewski.notebook.dto;

import androidx.room.Entity;
import androidx.room.Ignore;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class CheckboxNote extends Notebook {

    @Ignore
    private List<Checkbox> checkboxes = new ArrayList<>();

    public CheckboxNote() {
        setType(Type.CHECKBOX_NOTE);
    }
}
