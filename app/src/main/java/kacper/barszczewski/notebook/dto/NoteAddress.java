package kacper.barszczewski.notebook.dto;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class NoteAddress {

    @PrimaryKey
    private Long id;

    @ColumnInfo(name = "latitude")
    private Double latitude;

    @ColumnInfo(name = "longitude")
    private Double longitude;

    @ColumnInfo(name = "country")
    private String country;

    @ColumnInfo(name = "locality")
    private String locality;

    public NoteAddress() {

    }

    @Ignore
    public NoteAddress(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
