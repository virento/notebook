package kacper.barszczewski.notebook.dto;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Note {

    public static interface Type {
        public static final Integer TEXT_NOTE = 1;
        public static final Integer CHECKBOX_NOTE = 2;
    }

    @PrimaryKey
    private Long id;

    private Long noteId;

    private Integer type;
}
