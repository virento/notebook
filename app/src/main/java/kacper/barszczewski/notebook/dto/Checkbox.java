package kacper.barszczewski.notebook.dto;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Checkbox {

    @PrimaryKey
    private Long id;

    @ColumnInfo(name = "notebookId")
    private Long notebookId;

    @ColumnInfo(name = "text")
    private String text;

    @ColumnInfo(name = "checked", defaultValue = "false")
    private Boolean checked = false;

}
