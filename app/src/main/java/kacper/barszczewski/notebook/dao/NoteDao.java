package kacper.barszczewski.notebook.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import kacper.barszczewski.notebook.dto.Note;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM Note WHERE id = :id")
    Note getById(Long id);

    @Insert
    Long insert(Note note);

    @Delete
    void delete(Note note);

    @Query("SELECT COUNT(*) FROM Note WHERE id = :id")
    Long isNotePresent(Long id);
}
