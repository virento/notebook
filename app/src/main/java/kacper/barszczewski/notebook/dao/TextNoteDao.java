package kacper.barszczewski.notebook.dao;


import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import kacper.barszczewski.notebook.dto.TextNote;

@Dao
public interface TextNoteDao {

    @Query("SELECT * FROM TextNote WHERE id = :id")
    TextNote getById(Long id);


    @Query("SELECT * FROM TextNote")
    List<TextNote> getAll();

    @Query("SELECT COUNT(*) FROM TextNote")
    int size();
}
