package kacper.barszczewski.notebook.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import kacper.barszczewski.notebook.dto.Checkbox;
import kacper.barszczewski.notebook.dto.CheckboxNote;
import kacper.barszczewski.notebook.dto.NoteAddress;
import kacper.barszczewski.notebook.dto.TextNote;


@Dao
public interface NotebookDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(TextNote note);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(TextNote note);

    @Delete
    void delete(TextNote note);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(CheckboxNote note);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(CheckboxNote note);

    @Delete
    void delete(CheckboxNote note);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(NoteAddress note);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(NoteAddress note);

    @Delete
    void delete(NoteAddress note);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(Checkbox note);

    @Update
    void update(Checkbox note);

    @Delete
    void delete(Checkbox note);

    @Query("SELECT * FROM NoteAddress WHERE id = :id")
    NoteAddress getAddressById(Long id);

    @Query("SELECT COUNT(*) FROM NoteAddress WHERE id = :id")
    Long isAddressPresent(Long id);
}
