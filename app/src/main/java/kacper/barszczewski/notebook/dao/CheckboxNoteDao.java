package kacper.barszczewski.notebook.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import kacper.barszczewski.notebook.dto.Checkbox;
import kacper.barszczewski.notebook.dto.CheckboxNote;

@Dao
public interface CheckboxNoteDao {

    @Query("SELECT * FROM CheckboxNote WHERE id = :id")
    CheckboxNote getById(Long id);

    @Query("SELECT * FROM CheckboxNote")
    List<CheckboxNote> getAll();

    @Query("SELECT COUNT(*) FROM CheckboxNote")
    int size();

    @Query("SELECT * FROM Checkbox WHERE notebookId = :noteId")
    List<Checkbox> getCheckboxForNote(Long noteId);
}
