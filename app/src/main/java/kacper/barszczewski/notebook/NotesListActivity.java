package kacper.barszczewski.notebook;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kacper.barszczewski.notebook.database.DatabaseHelper;
import kacper.barszczewski.notebook.database.FirebaseDatabaseImpl;
import kacper.barszczewski.notebook.database.NotesInterface;
import kacper.barszczewski.notebook.database.RoomAppDatabaseImpl;
import kacper.barszczewski.notebook.database.SimpleDatabaseImpl;
import kacper.barszczewski.notebook.dto.CheckboxNote;
import kacper.barszczewski.notebook.dto.Notebook;
import kacper.barszczewski.notebook.dto.TextNote;

public class NotesListActivity extends AppCompatActivity {

    private static final String PREF_DB_TYPE = "kacper.barszczews.notebook.PREF_DB_TYPE";

    public static final int NOTE_ACTIVITY_CODE = 32131;
    public static final int RC_SIGN_IN = 23132;

    private RecyclerView notesView;

    private NotesInterface notesInterface;
    private NotesListAdapter adapter;

    private FloatingActionMenu fab;

    private FirebaseUser user = null;

    private DatabaseHelper.DatabaseType dbType;

    private SwipeRefreshLayout swipeRefreshLayout;

    private TextView noNotebooksText;

    private LinearLayout filterView;

    private Animation bottomUp, bottomDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_list);

        loadPreferences();

        setupDatabase();


        fab = findViewById(R.id.fab_add);
        fab.setClosedOnTouchOutside(true);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshView);
        filterView = findViewById(R.id.filter_view);
        filterView.setVisibility(View.INVISIBLE);
        filterView.setOnTouchListener((v, event) -> {
            super.onTouchEvent(event);
            return true;
        });
        bottomUp = AnimationUtils.loadAnimation(this, R.anim.bottom_up);
        bottomDown = AnimationUtils.loadAnimation(this, R.anim.bottom_down);
        bottomDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                filterView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        setupFilter();
        swipeRefreshLayout.setOnRefreshListener(this::syncNotes);
        swipeRefreshLayout.setRefreshing(true);

        notesView = findViewById(R.id.notebooksView);
        notesView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NotesListAdapter(this);
        notesView.setAdapter(adapter);

        FloatingActionButton fab1 = findViewById(R.id.fab_add_1);
        FloatingActionButton fab2 = findViewById(R.id.fab_add_2);
        fab1.setOnClickListener(v -> showNewNotebookActivity(new TextNote()));
        fab2.setOnClickListener(v -> showNewNotebookActivity(new CheckboxNote()));
        notesView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    fab.hideMenuButton(true);
                } else if (dy < 0) {
                    fab.showMenuButton(true);
                }
            }
        });

        switchDbTo(dbType);
    }

    private void setupFilter() {
        Button clearButton = findViewById(R.id.filter_clear);
        Button hideButton = findViewById(R.id.filter_hide);
        Spinner sortSpinner = findViewById(R.id.filter_sort);
        ChipGroup orders = findViewById(R.id.filter_order);
        ChipGroup types = findViewById(R.id.filter_type);
        List<String> sortOptions = new ArrayList<>();
        sortOptions.add(getString(R.string.edit_date));
        sortOptions.add(getString(R.string.title));
        sortOptions.add(getString(R.string.create_date));
        sortSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, sortOptions));
        orders.setOnCheckedChangeListener((chipGroup, i) -> {
            Chip chip1 = (Chip) chipGroup.getChildAt(0);
            adapter.setOrder(chip1.isChecked() ? 0 : 1);
        });
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSortBy(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                adapter.setSortBy(0);
            }
        });
        ((Chip) types.getChildAt(0)).setOnCheckedChangeListener(typeChipChecked(types));
        ((Chip) types.getChildAt(1)).setOnCheckedChangeListener(typeChipChecked(types));
        clearButton.setOnClickListener(v -> {
            sortSpinner.setSelection(0);
            orders.check(R.id.chip_desc);
            types.check(R.id.chip);
            types.check(R.id.chip2);
            adapter.clearFilters();
        });
        hideButton.setOnClickListener(v -> filterView.startAnimation(bottomDown));

    }

    private CompoundButton.OnCheckedChangeListener typeChipChecked(ChipGroup chipGroup) {
        return (buttonView, isChecked) -> {
            Chip chip1 = (Chip) chipGroup.getChildAt(0);
            Chip chip2 = (Chip) chipGroup.getChildAt(1);
            if (chip1.isChecked() && chip2.isChecked()) {
                adapter.setNotesType(2);
            } else {
                if (chip1.isChecked()) {
                    adapter.setNotesType(0);
                } else if (chip2.isChecked()) {
                    adapter.setNotesType(1);
                } else {
                    buttonView.setChecked(true);
                }
            }
        };
    }

    private void setupDatabase() {
        switch (dbType) {
            case FIREBASE:
                FirebaseDatabaseImpl.init(this);
                notesInterface = FirebaseDatabaseImpl.getInstance();
                return;
            case ROOM:
                RoomAppDatabaseImpl.init(this);
                notesInterface = RoomAppDatabaseImpl.getInstance();
                return;
            case SAMPLES:
                notesInterface = SimpleDatabaseImpl.getInstance();
        }
    }

    private void loadPreferences() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        int dbTypeInt = preferences.getInt(PREF_DB_TYPE, DatabaseHelper.DatabaseType.ROOM.getValue());
        this.dbType = DatabaseHelper.DatabaseType.fromValue(dbTypeInt);
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = FirebaseAuth.getInstance().getCurrentUser();
        refreshUserUi();
    }

    @Override
    public void onBackPressed() {
        if (fab.isOpened()) {
            fab.close(false);
            return;
        }
        if (filterView.getVisibility() == View.VISIBLE) {
            filterView.startAnimation(bottomDown);
            return;
        }
        super.onBackPressed();
    }

    private void showNewNotebookActivity(Notebook textNote) {
        fab.close(false);
        showNotebookActivity(textNote);
    }

    public void showNotebookActivity(Notebook notebook) {
        Class<?> clazz = null;
        if (notebook instanceof TextNote) {
            clazz = TextNoteActivity.class;
        } else if (notebook instanceof CheckboxNote) {
            clazz = CheckboxNoteActivity.class;
        } else {
            return;
        }
        Long id = notebook.getId();
        if (id == null) {
            id = -2L;
        }
        Intent intent = new Intent(getApplicationContext(), clazz);
        intent.putExtra(NoteActivityAbstract.NOTE_DB_TYPE, notesInterface.getType().getValue());
        intent.putExtra(NoteActivityAbstract.NOTE_ID, id);
        startActivityForResult(intent, NotesListActivity.NOTE_ACTIVITY_CODE);
    }

    private void refreshUserUi() {
        invalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem loginItem = menu.findItem(R.id.menu_login);
        MenuItem dbItem = menu.findItem(R.id.menu_switch_db);
        MenuItem importItem = menu.findItem(R.id.menu_import_local);
        if (user == null) {
            loginItem.setIcon(R.drawable.ic_person);
        } else {
            loginItem.setIcon(R.drawable.ic_logout);
        }
        if (user == null) {
            dbItem.setVisible(false);
        }
        if (dbType.equals(DatabaseHelper.DatabaseType.FIREBASE)) {
            dbItem.setTitle(R.string.local_db);
        } else {
            dbItem.setTitle(R.string.external_db);
        }
        if (dbType.equals(DatabaseHelper.DatabaseType.FIREBASE)) {
            importItem.setVisible(true);
        } else {
            importItem.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.filtrQuery(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length() == 0) {
                    adapter.filtrQuery("");
                    return true;
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_login:
                if (user == null) {
                    logInWithFirebase();
                } else {
                    startLogOutProcess();
                }
                return true;
            case R.id.menu_switch_db:
                if (dbType.equals(DatabaseHelper.DatabaseType.FIREBASE)) {
                    switchDbTo(DatabaseHelper.DatabaseType.ROOM);
                } else {
                    switchDbTo(DatabaseHelper.DatabaseType.FIREBASE);
                }
                refreshUserUi();
                return true;
            case R.id.menu_clear:
                Observable.just(notesInterface)
                        .doOnNext(NotesInterface::clear)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(integerObservable -> adapter.refreshItems());
                return true;
            case R.id.menu_import_local:
                new AlertDialog.Builder(this)
                        .setMessage(R.string.confirm_import_local_data)
                        .setPositiveButton(R.string.import_n, (dialog, which) -> {
                            RoomAppDatabaseImpl.init(this);
                            importFromTo(notesInterface, RoomAppDatabaseImpl.getInstance());
                        })
                        .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss())
                        .show();
                return true;
            case R.id.menu_map:
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                intent.putExtra(NoteActivityAbstract.NOTE_DB_TYPE, notesInterface.getType().getValue());
                startActivity(intent);
                return true;
            case R.id.menu_filter:
                if (filterView.getVisibility() == View.VISIBLE) {
                    filterView.startAnimation(bottomDown);
                } else {
                    filterView.startAnimation(bottomUp);
                    filterView.setVisibility(View.VISIBLE);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("CheckResult")
    private void importFromTo(NotesInterface notesInterface, RoomAppDatabaseImpl instance) {
        Observable.just(notesInterface)
                .doOnNext(nInterface -> nInterface.performImport(instance))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(nInterface -> {
                    Toast.makeText(this, R.string.import_complete, Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(true);
                    syncNotes();
                });
    }


    private void switchDbTo(DatabaseHelper.DatabaseType newDbType) {
        getPreferences(MODE_PRIVATE).edit().putInt(PREF_DB_TYPE, newDbType.getValue()).apply();
        swipeRefreshLayout.setRefreshing(true);
        this.dbType = newDbType;
        setupDatabase();
        adapter.setInterface(notesInterface);
        syncNotes();
    }

    @SuppressLint("CheckResult")
    private void syncNotes() {
        Observable.just(notesInterface)
                .doOnNext(NotesInterface::sync)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integerObservable -> {
                    adapter.refreshItems(() -> swipeRefreshLayout.setRefreshing(false));
                    swipeRefreshLayout.setRefreshing(false);
                });
    }

    private void startLogOutProcess() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.confirm)
                .setMessage(R.string.confirmLogout)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> logOutFromFirebase())
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss())
                .show();

    }

    private void logOutFromFirebase() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(command -> {
                    user = null;
                    if (dbType.equals(DatabaseHelper.DatabaseType.FIREBASE)) {
                        switchDbTo(DatabaseHelper.DatabaseType.ROOM);
                    }
                    refreshUserUi();
                    Toast.makeText(this, R.string.logout_success, Toast.LENGTH_LONG).show();
                });
    }

    private void logInWithFirebase() {
        List<AuthUI.IdpConfig> providers = Collections.singletonList(
                new AuthUI.IdpConfig.GoogleBuilder().build()
        );
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NOTE_ACTIVITY_CODE) {
            adapter.refreshItems();
        } else if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                this.user = FirebaseAuth.getInstance().getCurrentUser();
                Toast.makeText(this, R.string.login_success, Toast.LENGTH_SHORT).show();
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.error)
                        .setMessage(R.string.error_on_login)
                        .show();
            }
            refreshUserUi();
        }
    }
}
