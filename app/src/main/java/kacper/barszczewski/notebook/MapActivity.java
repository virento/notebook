package kacper.barszczewski.notebook;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Address;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.primitives.Doubles;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kacper.barszczewski.notebook.database.DatabaseHelper;
import kacper.barszczewski.notebook.database.NotesInterface;
import kacper.barszczewski.notebook.dto.CheckboxNote;
import kacper.barszczewski.notebook.dto.NoteAddress;
import kacper.barszczewski.notebook.dto.Notebook;
import kacper.barszczewski.notebook.dto.TextNote;

import static kacper.barszczewski.notebook.NoteActivityAbstract.NOTE_DB_TYPE;
import static kacper.barszczewski.notebook.NotesListActivity.NOTE_ACTIVITY_CODE;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private NotesInterface nInterface;

    private List<Notebook> list;

    private RecyclerView recycler;

    private ViewAdapter adapter;

    private Animation bottomUp, bottomDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        recycler = findViewById(R.id.list_view);
        recycler.setVisibility(View.INVISIBLE);

        recycler.setLayoutManager(new LinearLayoutManager(this));

        int noteDbType = getIntent().getIntExtra(NOTE_DB_TYPE, -1);
        if (noteDbType == -1) {
            finishActivity(0);
            return;
        }

        nInterface = DatabaseHelper.getInterface(DatabaseHelper.DatabaseType.fromValue(noteDbType));
        if (nInterface == null) {
            finish();
            return;
        }

        bottomUp = AnimationUtils.loadAnimation(this, R.anim.bottom_up);
        bottomDown = AnimationUtils.loadAnimation(this, R.anim.bottom_down);
        bottomDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                recycler.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Observable.just(nInterface)
                .doOnNext(nInterface -> list = nInterface.getItems())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(nInterface -> setupMarkers());
    }

    private void setupMarkers() {
        LatLng marker = null;
        for (int i = list.size() - 1; i >= 0; i--) {
            Notebook item = list.get(i);
            Double longtitude = item.getAddress().getLongitude();
            Double latitude = item.getAddress().getLatitude();
            if (latitude == null || longtitude == null) {
                continue;
            }
            marker = new LatLng(item.getAddress().getLatitude(), item.getAddress().getLongitude());
            mMap.addMarker(new MarkerOptions().position(marker).title(item.getTitle()));

        }
        mMap.setOnMarkerClickListener(marker1 -> {
            adapter = new ViewAdapter(new NoteAddress(marker1.getPosition().latitude, marker1.getPosition().longitude));
            recycler.setAdapter(adapter);
            recycler.startAnimation(bottomUp);
            recycler.setVisibility(View.VISIBLE);
            return true;
        });
        mMap.setOnMapClickListener(latLng -> {
            recycler.startAnimation(bottomDown);
            recycler.setVisibility(View.VISIBLE);
        });
        if (marker == null) {
            return;
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(marker));
    }

    public List<Notebook> getForAddress(NoteAddress address) {
        List<Notebook> notes = new ArrayList<>();
        for (Notebook notebook : list) {
            Double latitude = notebook.getAddress().getLatitude();
            Double longtitude = notebook.getAddress().getLongitude();
            if (latitude == null || longtitude == null) {
                continue;
            }
            if (Doubles.compare(address.getLatitude(), latitude) == 0
                    && Doubles.compare(address.getLongitude(), longtitude) == 0) {
                notes.add(notebook);
            }
        }
        return notes;
    }

    public void showNotebookActivity(Notebook notebook) {
        Class<?> clazz = null;
        if (notebook instanceof TextNote) {
            clazz = TextNoteActivity.class;
        } else if (notebook instanceof CheckboxNote) {
            clazz = CheckboxNoteActivity.class;
        } else {
            return;
        }
        Long id = notebook.getId();
        if (id == null) {
            id = -2L;
        }
        Intent intent = new Intent(getApplicationContext(), clazz);
        intent.putExtra(NoteActivityAbstract.NOTE_DB_TYPE, nInterface.getType().getValue());
        intent.putExtra(NoteActivityAbstract.NOTE_ID, id);
        startActivityForResult(intent, NOTE_ACTIVITY_CODE);
    }

    private class ViewAdapter extends RecyclerView.Adapter<MapViewHolder> {

        private List<Notebook> notebooks;

        public ViewAdapter(NoteAddress address) {
            if (address == null) {
                notebooks = new ArrayList<>();
            } else {
                notebooks = getForAddress(address);
            }
        }

        @NonNull
        @Override
        public MapViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MapViewHolder(getLayoutInflater().inflate(R.layout.note_map_list, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MapViewHolder holder, int position) {
            holder.bind(notebooks.get(position));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.itemView.setBackgroundColor(position % 2 == 0 ? getResources().getColor(R.color.note_map_dark, null) : getResources().getColor(R.color.note_map_light, null));
            } else {
                holder.itemView.setBackgroundColor(position % 2 == 0 ? getResources().getColor(R.color.note_map_dark) : getResources().getColor(R.color.note_map_light));
            }
        }

        @Override
        public int getItemCount() {
            return notebooks.size();
        }
    }

    private class MapViewHolder extends RecyclerView.ViewHolder {

        private TextView title;

        public MapViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_map_title);
        }

        public void bind(Notebook note) {
            title.setText(note.getTitle());
            itemView.setOnClickListener(v -> showNotebookActivity(note));
        }
    }
}
