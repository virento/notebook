package kacper.barszczewski.notebook;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import kacper.barszczewski.notebook.database.FirebaseDatabaseImpl;
import kacper.barszczewski.notebook.database.NotesInterface;
import kacper.barszczewski.notebook.database.RoomAppDatabaseImpl;
import kacper.barszczewski.notebook.dto.Checkbox;
import kacper.barszczewski.notebook.dto.CheckboxNote;
import kacper.barszczewski.notebook.dto.TextNote;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class RoomDatabaseTest {

    private NotesInterface nInterface;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        RoomAppDatabaseImpl.init(context);
//        nInterface = RoomAppDatabaseImpl.getInstance();
        nInterface = FirebaseDatabaseImpl.getInstance();
//        RoomAppDatabaseImpl.init(context);
    }

    @After
    public void closeDb() {
        nInterface.close();
    }

    @Test
    public void writeTextNoteAndRead() throws InterruptedException {
        String title = "Test " + System.currentTimeMillis();
        String body = "Body " + System.currentTimeMillis();
        Date date = new Date();
        String country = "C" + System.currentTimeMillis();

        TextNote textNote = new TextNote();
        textNote.setTitle(title);
        textNote.setBody(body);
        textNote.setCreateDate(date);
        textNote.getAddress().setCountry(country);
        textNote.getAddress().setLatitude(1000D);
        textNote.getAddress().setLongitude(2000D);

        nInterface.save(textNote);

        Thread.sleep(1000);
        TextNote dbNote = (TextNote) nInterface.getNoteById(textNote.getId());

        assertEquals(textNote.getId(), dbNote.getId());
        assertEquals(textNote.getTitle(), dbNote.getTitle());
        assertNotNull(dbNote.getAddress());
        assertEquals(textNote.getAddress().getId(), dbNote.getAddress().getId());
        assertEquals(textNote.getAddress().getCountry(), dbNote.getAddress().getCountry());
        assertEquals(textNote.getAddress().getLatitude(), dbNote.getAddress().getLatitude());
        assertEquals(textNote.getAddress().getLongitude(), dbNote.getAddress().getLongitude());
        assertEquals(textNote.getTitle(), title);
        assertEquals(textNote.getBody(), body);
        assertEquals(textNote.getCreateDate(), date);
        assertEquals(textNote.getAddress().getCountry(), country);
    }

    @Test
    public void writeCheckboxNoteAnRead() {
        String title = "Test " + System.currentTimeMillis();
        Date date = new Date();
        String country = "C" + System.currentTimeMillis();

        CheckboxNote note = new CheckboxNote();
        note.setTitle(title);
        note.setCreateDate(date);
        note.getAddress().setCountry(country);
        for (int i = 0; i < 2; i++) {
            Checkbox box = new Checkbox();
            box.setText("Test " + i);
            box.setChecked(i % 2 == 0);
            note.getCheckboxes().add(box);
        }

        nInterface.save(note);

        CheckboxNote dbNote = (CheckboxNote) nInterface.getNoteById(note.getId());

        assertEquals(note.getId(), dbNote.getId());
        assertEquals(note.getTitle(), dbNote.getTitle());
        assertNotNull(dbNote.getAddress());
        assertEquals(note.getAddress().getId(), dbNote.getAddress().getId());
        assertEquals(note.getAddress().getCountry(), dbNote.getAddress().getCountry());
        assertNotNull(dbNote.getCheckboxes());
        assertEquals(note.getCheckboxes().get(0).getChecked(), dbNote.getCheckboxes().get(0).getChecked());
        assertEquals(note.getCheckboxes().get(0).getText(), dbNote.getCheckboxes().get(0).getText());
        assertEquals(note.getCheckboxes().get(0).getId(), dbNote.getCheckboxes().get(0).getId());
        assertEquals(note.getCheckboxes().get(1).getChecked(), dbNote.getCheckboxes().get(1).getChecked());
        assertEquals(note.getCheckboxes().get(1).getText(), dbNote.getCheckboxes().get(1).getText());
        assertEquals(note.getCheckboxes().get(1).getId(), dbNote.getCheckboxes().get(1).getId());
        assertEquals(note.getTitle(), title);
        assertEquals(note.getCreateDate(), date);
        assertEquals(note.getAddress().getCountry(), country);

    }
}
